README.txt
==========
Provides API to handle forms with AJAX.

HOW TO USE
=========
1) Implement hook_form_alter() in your module.

2) For certain form add following code, example:
function my_module_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'article_node_form') {
    // Make form as AJAX form.
    $form['#ajax_api'] = TRUE;
    // Message which will be displayed after succesful submitting.
    $form['#ajax_api_success_message'] = t('AJAX API message.');
  }
}

3) Enjoy. Your form will be automatically validated via AJAX,
after successful submitting you will see defined message.
