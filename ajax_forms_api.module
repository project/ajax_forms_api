<?php
/**
 * @file
 * Provides API to handle forms with AJAX.
 */

/**
 * Implements hook_menu().
 */
function ajax_forms_api_menu() {
  $items = array();

  $items['ajax/ajax-forms-api'] = array(
    'page callback' => 'ajax_forms_api_callback',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'delivery callback' => 'ajax_deliver',
    'theme callback' => 'ajax_base_page_theme',
  );

  return $items;
}

/**
 * Implements hook_form_alter().
 */
function ajax_forms_api_form_alter(&$form, &$form_state, $form_id) {
  if (!empty($form['#ajax_api'])) {
    $form['messages'] = array(
      '#markup' => '<div id="ajax-forms-api-messages"></div>',
      '#weight' => -999,
    );
    $form['actions']['submit']['#ajax'] = array(
      'path' => 'ajax/ajax-forms-api',
    );
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function ajax_forms_api_module_implements_alter(&$implementations, $hook) {
  // Call to hook_form_alter in the end.
  if ($hook == 'form_alter') {
    $group = $implementations['ajax_forms_api'];
    unset($implementations['ajax_forms_api']);
    $implementations['ajax_forms_api'] = $group;
  }
}

/**
 * Process ajax forms.
 */
function ajax_forms_api_callback() {
  // Process form.
  list($form, $form_state) = ajax_get_form();
  drupal_process_form($form['#form_id'], $form, $form_state);

  $commands = array();
  if ($form_state['executed']) {
    // Reload page if form successfully submitted.
    ctools_include('ajax');
    $commands[] = ctools_ajax_command_reload();

    // Set success message if exist.
    if (!empty($form['#ajax_api_success_message'])) {
      drupal_set_message($form['#ajax_api_success_message']);
    }
  }
  else {
    // Show messages.
    $commands[] = ajax_command_html('#ajax-forms-api-messages', theme('status_messages'));
  }
  return array('#type' => 'ajax', '#commands' => $commands);
}
